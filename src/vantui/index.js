import {
  Field,
  Cell,
  CellGroup,
  Button,
  Icon,
  Tabbar,
  TabbarItem,
  Search,
  Swipe, SwipeItem,
  Grid, GridItem,
  NoticeBar,
  NavBar,
  Tab, Tabs,
  List,
  PullRefresh,
  ImagePreview,
  CountDown,
  ActionBar, ActionBarIcon, ActionBarButton,
  Popup,
  Lazyload,
  Empty,
  Checkbox, CheckboxGroup,
  Stepper,
  SubmitBar,
  Tag
} from 'vant'

const componentList = [
  Field,
  Cell,
  CellGroup,
  Button,
  Icon,
  Tabbar,
  TabbarItem,
  Search,
  Swipe, SwipeItem,
  Grid, GridItem,
  NoticeBar,
  NavBar,
  Tab, Tabs,
  List,
  PullRefresh,
  ImagePreview,
  CountDown,
  ActionBar, ActionBarIcon, ActionBarButton,
  Popup,
  Lazyload,
  Empty,
  Checkbox, CheckboxGroup,
  Stepper,
  SubmitBar,
  Tag
]

export function registerVantComp(app) {
  componentList.forEach((comp) => {
    app.use(comp)
  })
}
