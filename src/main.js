import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import 'normalize.css/normalize.css'
import './whole/whole.css'
import './whole/iconfont/iconfont.css'
import './whole/properties.css'

import 'lib-flexible/flexible'


const app=createApp(App)

// 按需注册 vant 组件
import { registerVantComp } from './vantui'
registerVantComp(app)

app.use(store).use(router)
app.mount('#app')

