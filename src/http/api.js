import ajax from './ajax'
const BASE_URL = 'https://www.fastmock.site/mock/dec4b2f272d3e6e341183f1e982ef5dc/api'
// https://www.opposhop.cn/cn/oapi

// 经过处理的 fastmock
export const reqCartList=()=>ajax(BASE_URL+'/orders/web/cart/v1/list')
export const swipeTextList=()=>ajax(BASE_URL+'/swipe_text')
export const reqHome=()=>ajax(BASE_URL+'/home')
export const reqHomeModule=()=>ajax(BASE_URL+'/home_module')
export const reqHomeSwipe=()=>ajax(BASE_URL+'/home_swipe')
export const reqHomeNav=()=>ajax(BASE_URL+'/home_nav')

export const reqCategoryLeft=()=>ajax(BASE_URL+'/category_left')
export const reqCategoryRight1=()=>ajax(BASE_URL+'/category_right_1')
export const reqCategoryRight2=()=>ajax(BASE_URL+'/category_right_2')
export const reqCategoryRight3=()=>ajax(BASE_URL+'/category_right_3')

export const reqSkuId=()=>ajax(BASE_URL+'/product')
// export const reqSkuId=()=>ajax(BASE_URL+'https://www.opposhop.cn/cn/oapi/goods/web/info/skuId?skuId=3471')
export const reqSearchRecommend=()=>ajax(BASE_URL+'/search_recomond')
export const reqSearchResult=()=>ajax(BASE_URL+'/search_result')
export const reqLogin=(loginForm)=>ajax(BASE_URL+'/login',loginForm,'POST')