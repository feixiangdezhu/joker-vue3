import { createRouter, createWebHashHistory } from 'vue-router'

import Home from '../views/Home'
import Search from '../views/Search'
import Category from '../views/Category'
import ProductDetail from '../views/Product/detail.vue'
import Cart from '../views/cart/Cart.vue'
import Ucenter from '../views/Ucenter/Ucenter.vue'
import Login from '../views/Login/Login.vue'

// component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/search',
    name: 'Search',
    component: Search
  },
  {
    path: '/category',
    name: 'Category',
    component: Category
  },
  {
    path: '/product',
    name: 'Product',
    component: ProductDetail
  },
  {
    path: '/cart',
    name: 'Cart',
    component: Cart,
    meta:{needLogin:true}
  },
  {
    path: '/ucenter',
    name: 'Ucenter',
    component: Ucenter,
    meta:{needLogin:true}
  },
  {
    path: '/login',
    name: 'Login',
    component: Login
  }

]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})

router.beforeEach((to,from,next)=>{
  const token=sessionStorage.getItem('token')
  if(to.meta.needLogin && !token){
    next({path:"/login"})
  } else {
   next()
  }
 })

export default router
